<?php
class VencimientoController extends BaseController {
 
 
    /**
     * Mustra la lista con todos los usuarios
     */
    public function show()
    {
        $vencimientos = Vencimiento::all();
        $categorias = Categoriavencimiento::all();
        

        
        return View::make('vencimiento.show')->with("vencimientos",$vencimientos)->with("categorias",$categorias);
        

    }


     public function insert()
    {

        //return "EN DESARROLLO";
        $vencimiento = new Vencimiento; 
        $personal = Personal::lists("nombre","id");
        $tipo_documento = Array(
            "1"=>"Licencia",
            "2"=>"Exámen pre ocupacional",
            "3"=>"Contrato",
            "4"=>"Mantencion",
            "5"=>"Otros"
        );

        $categorias = Categoriavencimiento::lists("nombre","id");

        
        return View::make('vencimiento.formulario')
        ->with("vencimiento",$vencimiento)->with("tipo_documento",$tipo_documento)->with("categorias",$categorias);
    }
 
 
    /**
     * Crear el usuario nuevo
     */
    public function insert2()
    {

        $ven = new Vencimiento;


        $datos = Input::all(); 


        //return $datos;

        $random = rand(0,99999);
        
        if ($ven->isValid($datos))
        {
             
             list($dia,$mes,$ano) = explode("/",$datos['fecha_vencimiento']);
            $datos['fecha_vencimiento'] = "$ano-$mes-$dia";

            
            if (Input::hasFile("archivo"))
                {
                    $adjunto1 = Input::file('archivo');
                    $datos["archivo"] = $random."_".str_replace(" ","_",$adjunto1->getClientOriginalName());
                    $adjunto1->move("archivos/vencimiento",$random."_".str_replace(" ","_",$adjunto1->getClientOriginalName()));
                    
                   
                }


            $ven->fill($datos);
            // Guardamos el usuario
            /* $usuario->password = Hash::make($usuario->password);*/

          
      
            
           $ven->save();

            return Redirect::to('vencimiento')->with("mensaje","Datos Ingresados correctamente");
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
return Redirect::to('vencimiento/insert')->withInput()->withErrors($ven->errors);
            //return "mal2";
        }
     //   return Redirect::to('usuarios');
    // el método redirect nos devuelve a la ruta de mostrar la lista de los usuarios
 
    }
 
     /**
     * Ver usuario con id
     */

    public function update($id) //get
    {
        //echo $id;
      
            //return "EN DESARROLLO";
           $vencimiento = Vencimiento::find($id);
           $tipo_documento = Array(
            "1"=>"Licencia",
            "2"=>"Exámen pre ocupacional",
            "3"=>"Contrato",
            "4"=>"Mantencion",
            "5"=>"Otros"
        );

           $categorias = Categoriavencimiento::lists("nombre","id");
   
        return View::make('vencimiento.formulario')->with("vencimiento", $vencimiento)
        ->with("categorias",$categorias);
 
                
 
      
    }


    public function update2($id) //post
    {
        
         $vencimiento = Vencimiento::find($id);
 $datos = Input::all(); 

$random = rand(0,99999);
       
        
        if ($vencimiento->isValid($datos))
        {

            list($dia,$mes,$ano) = explode("/",$datos['fecha_vencimiento']);
            $datos['fecha_vencimiento'] = "$ano-$mes-$dia";
            
            if (Input::hasFile("archivo"))
                {
                    $adjunto1 = Input::file('archivo');
                    $datos["archivo"] = $random."_".str_replace(" ","_",$adjunto1->getClientOriginalName());
                    $adjunto1->move("archivos/vencimiento",$random."_".str_replace(" ","_",$adjunto1->getClientOriginalName()));
                    
                   
                }

                if($datos["archivo"] == null){
                    $datos = array_diff($datos, [null]);
                }
                
                //return $datos;

            $vencimiento->fill($datos);


      
            
           $vencimiento->save();

            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            //return Redirect::action('ClienteController@show');
           return Redirect::to('vencimiento')->with("mensaje","Datos actualizados correctamente");

            
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
return Redirect::to('vencimiento/update/'.$id)->withInput()->withErrors($vencimiento->errors);
            //return "mal2";
        }

        return Redirect::to('vencimiento')->with("mensaje","NO");
      
    }



    public function eliminar()
    {
        $id = Input::get('id'); //acedemos a la variable id traida por AJAX ($.get)
        $vencimiento = Vencimiento::find($id);

        $vencimiento->delete();

    //return Redirect::to('usuarios/insert');
    }


    public function mostrar()
    {

             $id = Input::get("id");
 $medica = Vencimiento::find($id);
           $personal = Personal::lists("nombre","id");
   
        return View::make('medica.mostrar')->with("medica", $medica)
        ->with("personal",$personal);
 
    }



 






}



?>