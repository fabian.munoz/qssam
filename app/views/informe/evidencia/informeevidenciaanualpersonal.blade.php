@extends('layouts.master')

@section('contenido')

            

 <h3 class="header smaller lighter">Evidencias Anual: 
                
    </h3>



<div class="row">
  <div class="col-xs-12">
      <div class="alert alert-block alert-success">

      {{ Form::open(array('url' => "informeevidenciaanualpersonal", "method"=>"get")) }}
            <div class="form-group">
            

            {{Form::select('ano',array("2015"=>"2015","2016"=>"2016","2017"=>"2017","2018"=>"2018"), $data["ano"])}}
            {{Form::select('personal',$personals, $data["personal"])}}
            

            {{Form::submit()}}

      {{ Form::close()}}
            </div>
      </div>
</div>
</div>

<div class="row">
  <div id="buttons"></div>
  <div class="col-xs-12">
       <table id="example" class="table table-striped table-bordered table-hover">

                        <thead>
                          <tr>
                            <th>Mes</th>
                            <th>Abiertas</th>
                            <th>Cerradas</th>
                            <th>Atrasadas</th>
                        
                            

                           
                          </tr>
                        </thead>

                         <tfoot>
                        <tr>
                          <th>Mes</th>
                          <th>Abiertas</th>
                            <th>Cerradas</th>
                            <th>Atrasadas</th>

                  
                           
                        </tr>
                    </tfoot>


                        <tbody>

                        </tbody>



    </table>
  </div>
</div>

   
<script type="text/javascript">
    
     $(document).ready(function() {

      $( "#informeactive" ).addClass( "active" );


      //var ctx = document.getElementById("myChart1").getContext("2d");


 var data = {
    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
    datasets: [
        {
            label: "Abiertas",
            fillColor: "#FA5858",
          //  strokeColor: "rgba(220,220,220,0.8)",
           // highlightFill: "rgba(220,220,220,0.75)",
           // highlightStroke: "rgba(220,220,220,1)",
            data: [{{$abiertas["0"]}},{{$abiertas["1"]}},{{$abiertas["2"]}},{{$abiertas["3"]}},{{$abiertas["4"]}},{{$abiertas["5"]}},{{$abiertas["6"]}},{{$abiertas["7"]}},{{$abiertas["8"]}},{{$abiertas["9"]}},{{$abiertas["10"]}},{{$abiertas["11"]}}]
          
        },

         {
            label: "Cerradas",
            fillColor: "#000000",
          //  strokeColor: "rgba(220,220,220,0.8)",
           // highlightFill: "rgba(220,220,220,0.75)",
           // highlightStroke: "rgba(220,220,220,1)",
            data: [{{$cerradas["0"]}},{{$cerradas["1"]}},{{$cerradas["2"]}},{{$cerradas["3"]}},{{$cerradas["4"]}},{{$cerradas["5"]}},{{$cerradas["6"]}},{{$cerradas["7"]}},{{$cerradas["8"]}},{{$cerradas["9"]}},{{$cerradas["10"]}},{{$cerradas["11"]}}]
          
        },

         {
            label: "Atrasadas",
            fillColor: "yellow",
          //  strokeColor: "rgba(220,220,220,0.8)",
           // highlightFill: "rgba(220,220,220,0.75)",
           // highlightStroke: "rgba(220,220,220,1)",
            data: [{{$atrasadas["0"]}},{{$atrasadas["1"]}},{{$atrasadas["2"]}},{{$atrasadas["3"]}},{{$atrasadas["4"]}},{{$atrasadas["5"]}},{{$atrasadas["6"]}},{{$atrasadas["7"]}},{{$atrasadas["8"]}},{{$atrasadas["9"]}},{{$atrasadas["10"]}},{{$atrasadas["11"]}}]
          
        }
    ]
};

var options = {
  legendTemplate : '<ul>'
                  +'<% for (var i=0; i<datasets.length; i++) { %>'
                    +'<li>'
                    +'<span style=\"background-color:<%=datasets[i].fillColor%>\">  &nbsp;&nbsp;  &nbsp;&nbsp;</span>'
                    +'<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                  +'</li>'
                +'<% } %>'
              +'</ul>',

              animation: false,

  };


/*
var myBarChart = new Chart(ctx).Bar(data,options);
var legend = myBarChart.generateLegend();


var wa = ctx.canvas.toDataURL();

    $("#img64").val(wa);

  //and append it to your page somewhere
  $('#chart1').append(legend);

*/

  var table = $('#example').DataTable({
    buttons: [
          'copyHtml5', 'excelHtml5'
      ],
    "columns": [
                        { "data": "param1"},
                        { "data": "param2"},
                        { "data": "param3"},
                        { "data": "param4"},

            ], 
    });



  for(var i=0; i<data.labels.length;i++){
    console.log(data);
    console.log(data.labels[i]);

    table.row.add( {
          "param1": data.labels[i],
          "param2":  data.datasets[0].data[i],
          "param3":  data.datasets[1].data[i],
          "param4":  data.datasets[2].data[i]
      } ).draw();
  }


  table.buttons().container().appendTo("#buttons");


});

</script>
    

@stop