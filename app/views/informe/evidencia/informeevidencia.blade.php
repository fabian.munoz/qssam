@extends('layouts.master')

@section('contenido')

            

 <h3 class="header smaller lighter">Actividades Abiertas vs Actividades Cerradas 
                
    </h3>


<?php
$meses = array(1=>"Enero",2=>"Feberero", 3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
$mesnombre = $meses[$data["mes"]];
?>


<div class="row">
  <div class="col-xs-12">
      <div class="alert alert-block alert-success">

      {{ Form::open(array('url' => "informeevidencia", "method"=>"get")) }}
            <div class="form-group">
            {{Form::label('', 'Fecha',array("class"=>"col-sm-3 control-label no-padding-right"))}}
            {{Form::select('mes',$meses, $data["mes"])}}

            {{Form::select('ano',array("2015"=>"2015","2018"=>"2018"), $data["ano"])}}
            {{Form::submit()}}

      {{ Form::close()}}
            </div>
      </div>
</div>
</div>

<div class="row">
  <div id="buttons"></div>
  <div class="col-xs-12">
       <table id="example" class="table table-striped table-bordered table-hover">

                        <thead>
                          <tr>
                            <th>Mes</th>
                            <th>Abiertas</th>
                            <th>Cerradas</th>
                        
                            

                           
                          </tr>
                        </thead>

                         <tfoot>
                        <tr>
                          <th>Mes</th>
                          <th>Abiertas</th>
                            <th>Cerradas</th>

                  
                           
                        </tr>
                    </tfoot>


                        <tbody>

                        </tbody>



    </table>
  </div>
</div>



   
<script type="text/javascript">
    
     $(document).ready(function() {

$( "#informeactive" ).addClass( "active" );

      

    // var ctx = document.getElementById("myChart1").getContext("2d");


 var data = {
    labels: ["{{$mesnombre}}"],
    datasets: [
        {
            label: "Actividades Abiertas",
            fillColor: "#FA5858",
          //  strokeColor: "rgba(220,220,220,0.8)",
           // highlightFill: "rgba(220,220,220,0.75)",
           // highlightStroke: "rgba(220,220,220,1)",
            data: [{{$abiertas}}]
        },
        {
            label: "Actividades Cerradas",
            fillColor: "#5882FA",
           // strokeColor: "rgba(151,187,205,0.8)",
           // highlightFill: "rgba(151,187,205,0.75)",
           // highlightStroke: "rgba(151,187,205,1)",
            data: [{{$cerradas}}]
        }
    ]
};

var options = {
  legendTemplate : '<ul>'
                  +'<% for (var i=0; i<datasets.length; i++) { %>'
                    +'<li>'
                    +'<span style=\"background-color:<%=datasets[i].fillColor%>\">  &nbsp;&nbsp;  &nbsp;&nbsp;</span>'
                    +'<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                  +'</li>'
                +'<% } %>'
              +'</ul>',

              animation: false
  };


/*
var myBarChart = new Chart(ctx).Bar(data,options);
var legend = myBarChart.generateLegend();

var wa = ctx.canvas.toDataURL();

    $("#img64").val(wa);

  //and append it to your page somewhere
  $('#chart1').append(legend);
*/

  var table = $('#example').DataTable({
    buttons: [
          'copyHtml5', 'excelHtml5'
      ],
    "columns": [
                        { "data": "param1"},
                        { "data": "param2"},
                        { "data": "param3"},

            ], 
    });



  for(var i=0; i<data.labels.length;i++){
    console.log(data);
    console.log(data.labels[i]);

    table.row.add( {
          "param1": data.labels[i],
          "param2":  data.datasets[0].data[i],
          "param3":  data.datasets[1].data[i]
      } ).draw();
  }


  table.buttons().container().appendTo("#buttons");




});

</script>
    

@stop