@extends('layouts.master')

@section('contenido')

            

 <h3 class="header smaller lighter">Productos sin devolucion
                
    </h3>


<?php

?>


<div class="row">
  <div class="col-xs-12">
      <div class="alert alert-block alert-success">

      {{ Form::open(array('url' => "informesindevolucion", "method"=>"get")) }}
            <div class="form-group">
            {{Form::label('', 'Bodega',array("class"=>"col-sm-3 control-label no-padding-right"))}}
            {{Form::select('bodegaid',$bodegas,$data['bodegaid'])}}

            {{Form::submit()}}

      {{ Form::close()}}
            </div>
      </div>
</div>
</div>


<div class="row">
  <div id="buttons"></div>
  <div class="col-xs-12">
       <table id="example" class="table table-striped table-bordered table-hover">

                        <thead>
                          <tr>
                            
                            <th>Producto</th>
                            <th>Stock</th>
                        
                            

                           
                          </tr>
                        </thead>

                         <tfoot>
                        <tr>
                          <th>Producto</th>
                          <th>Stock</th>
                  
                           
                        </tr>
                    </tfoot>


                        <tbody>

                        </tbody>



    </table>
  </div>
</div>







   
<script type="text/javascript">
    
     $(document).ready(function() {

$( "#informeactive" ).addClass( "active" );

      

    // var ctx = document.getElementById("myChart1").getContext("2d");


 var data = {
    labels: {{$productos}},
    datasets: [
        {
            label: "Cantidad",
            fillColor: "#FA5858",
          //  strokeColor: "rgba(220,220,220,0.8)",
           // highlightFill: "rgba(220,220,220,0.75)",
           // highlightStroke: "rgba(220,220,220,1)",
            data: {{$stock}}
        }
    ]
};

var options = {
  legendTemplate : '<ul>'
                  +'<% for (var i=0; i<datasets.length; i++) { %>'
                    +'<li>'
                    +'<span style=\"background-color:<%=datasets[i].fillColor%>\">  &nbsp;&nbsp;  &nbsp;&nbsp;</span>'
                    +'<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                  +'</li>'
                +'<% } %>'
              +'</ul>',

              animation: false
  };


/*
var myBarChart = new Chart(ctx).Bar(data,options);
var legend = myBarChart.generateLegend();

var wa = ctx.canvas.toDataURL();

    $("#img64").val(wa);

  //and append it to your page somewhere
  $('#chart1').append(legend);
*/



  var table = $('#example').DataTable({
    buttons: [
          'copyHtml5', 'excelHtml5'
      ],
    "columns": [
                        { "data": "param1"},
                        { "data": "param2"},
            ], 
    });



  for(var i=0; i<data.labels.length;i++){
    console.log();
    console.log(data.labels[i]);

    table.row.add( {
          "param1": data.labels[i],
          "param2":  data.datasets[0].data[i]
      } ).draw();
  }


  table.buttons().container().appendTo("#buttons");



});

</script>
    

@stop