@extends('layouts.master')
 



@section('contenido')





<div class="col-xs-12">

                    <h3 class="header smaller lighter">Archivo: 
                    <a href="{{URL::to('archivo/insert')}}"  class="btn btn-white btn-info btn-bold"> 
    <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Agregar</a>
    </h3>



                    <div class="clearfix">
                      <div class="pull-right tableTools-container"></div>
                    </div>
                    <div class="table-header">
                      Resultados
                    </div>
        
 
<table id="example" class="table table-striped table-bordered table-hover">
<div class="info"></div>
  <thead>
          <tr>
          <th>Nombre Documento</th>
                            <th>Categoria</th>
                            <th>Codigo</th>
                            <th>Version</th>
                            <th>Tiempo Vigencia</th>
                            <th>Fecha Elaboracion</th>
          
  <th>Acciones</th>
            
          </tr>
        </thead>
        <tbody>


  @foreach($archivos as $archivo)
           <tr>
           <td>{{ $archivo->nombre}}</td>
                            <td>{{$archivo->categoria->nombre}}</td>
                            <td>{{$archivo->codigo}}</td>
                            <td>{{$archivo->version}}</td>
                            <td>{{$archivo->tiempo}}</td>
                            <td>{{ date_format(date_create($archivo->elaboracion), 'd/m/Y')}}
         

  <td class="td-actions">
                       
                      
                          <a class="blue bootbox-mostrar" data-id={{$archivo->id}}>
                            <i class="fa fa-search-plus bigger-130"></i>
                          </a>


                          <a class="green" href= {{ 'archivo/update/'.$archivo->id }}>
                            <i class="fa fa-pencil bigger-130"></i>
                          </a>

                         <a class="red bootbox-confirm" data-id={{ $archivo->id }}>
                            <i class="fa fa-trash bigger-130"></i>
                          </a>


                            <a data-toggle="modal" class="botoncito" data-urlarchivo="https://docs.google.com/viewer?url=http://35.182.137.28/qssam/public/archivos/biblioteca/{{$archivo->archivo}}&embedded=true"  href="#" >
                                  <span class="label label-success arrowed">Vista Previa</span>
                                </a>
                                

                               

{{
link_to_asset('archivos/biblioteca/'.$archivo->archivo, "Descargar", array("class"=>"label label-warning arrowed"))
}}
                               
                      </td>
</tr>
          @endforeach
        </tbody>
  </table>

  </div>



      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Vista Previa</h4>
        </div>
        <div class="modal-body">

        
       
        
       <iframe id="iframe" src="" width="550" height="300" style="border: none;"></iframe> </div>
        
        <div class="modal-footer">
      
          
        </div>
      </div>
    </div>
  </div>



  <script type="text/javascript">
 $(document).ready(function() {


var table = $('#example').DataTable( {
      
       "language": {
                "url": "datatables.spanish.json"
            }
    } );




$( "#bibliotecaactive" ).addClass( "active" );
$( "#archivoactive" ).addClass( "active" );




$(".bootbox-confirm").on(ace.click_event, function() {
  var id = $(this).data('id');
var tr = $(this).parents('tr'); 

          bootbox.confirm("Deseas Eliminar el registro "+id, function(result) {
            if(result) { // si se seleccion OK
              
           
             
             $.get("{{ url('archivo/eliminar')}}",
              { id: id },

              function(data,status){ tr.fadeOut(1000); }
).fail(function(data){bootbox.alert("No se puede eliminar un registro padre: una restricción de clave externa falla");});

     
            }
           
          });
        });


$(".bootbox-mostrar").on(ace.click_event, function() {
  var id = $(this).data('id');

 $.get("{{ url('archivo/mostrar')}}",
              { id: id },
              function(data)
              { 
                bootbox.dialog({message: data});

              });
          
             
         


     
            
           
          });
     


$(".botoncito").click(function(){

  var urlarchivo = $(this).data('urlarchivo');
  

  $("#urlarchivo").val(urlarchivo);


        $('#iframe').attr('src', urlarchivo);
        //$('#iframe').reload();

  $('#myModal').modal("show");
});





}); // fin ready
 </script>




        

        


@stop

